# Manga Scraper
Mobile application to search and read manga.

## Getting Started
### Requirements
- [flutter](https://flutter.dev/docs/get-started/install)
### Compilation
To compile project to installable apk file go to project's main directory and type:
```
flutter build apk
```
Installable apk file should be in ```build/app/outputs/apk/release/```

## Authors
- [ketrab2003](https://gitlab.com/ketrab2003)