import 'package:flutter/cupertino.dart';
import 'package:flutter_cache_manager/flutter_cache_manager.dart';
import 'package:dio/dio.dart';
import 'package:dio_http_cache/dio_http_cache.dart';

// helper function
final RegExp uriSanitizer = RegExp(r'[^a-z0-9]+');
String sanitizeUri(String uri) {
  return uri.replaceAll(uriSanitizer, '-');
} 

// Manager for series' covers
class CoverCacheManager {
  static final Map<String, ImageProvider> _cache = <String, ImageProvider>{};

  static ImageProvider? getImage(String url) {
    if (_cache.containsKey(url)) return _cache[url];
    return null;
  }

  static void setImage(String url, ImageProvider p) {
    _cache[url] = p;
  }

  static const key = 'coverCacheKey';
  static CacheManager instance = CacheManager(
    Config(
      key,
      stalePeriod: const Duration(days: 365),
      maxNrOfCacheObjects: 10000000,
      repo: JsonCacheInfoRepository(databaseName: key),
    ),
  );
}

// Factory of CacheManagers for every series by url
class SeriesCacheManager {
  final String url;
  bool mute = false;

  // _cache is library-private, thanks to
  // the _ in front of its name.
  static final Map<String, SeriesCacheManager> _cache =
      <String, SeriesCacheManager>{};

  factory SeriesCacheManager(String url) {
    return _cache.putIfAbsent(url, () => SeriesCacheManager._internal(url));
  }

  String? key;
  CacheManager? instance;

  SeriesCacheManager._internal(this.url) {
    this.key = sanitizeUri('${this.url}CacheKey');
    this.instance = CacheManager(
      Config(
        this.key!,
        stalePeriod: const Duration(days: 30),
        maxNrOfCacheObjects: 200,
        repo: JsonCacheInfoRepository(databaseName: this.key),
      ),
    );
  }
}

class DioWithCache {
  static Dio? _instance;

  static Dio get instance {
    if (_instance != null) {
      return _instance!;
    }
    _instance = Dio();
    _instance!.interceptors.add(
      DioCacheManager(
        CacheConfig(
          defaultMaxAge: Duration(days: 30),
          maxMemoryCacheCount: 10000,
        )
      ).interceptor
    );

    return _instance!;
  }
}