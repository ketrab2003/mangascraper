import 'package:sqflite/sqflite.dart';
import 'package:path/path.dart' as p;

import 'package:scraper/logic/scraper_interfaces.dart';

import 'package:scraper/logic/gdrive_api.dart';
import 'package:googleapis/drive/v3.dart' as drive;

import 'dart:io';

import 'package:logger/logger.dart';

var logger = Logger(
  printer: PrettyPrinter(),
);

class DBManager {
  static String _dbFilename = "series.db"; // database's filename

  static List<Database?> _databases = <Database?>[null]; // singleton pattern

  static Future<Database> get database async {
    if (_databases[0] != null) return _databases[0]!;
    await loadDatabase();
    return _databases[0]!;
  }

  static Future<String> dbFilepath() async {
    String databasesPath = await getDatabasesPath();
    return p.join(databasesPath, _dbFilename);
  }

  static Future<void> loadDatabase() async {
    _databases[0] = await openDatabase(await dbFilepath(), version: 5,
        onCreate: (Database db, int version) async {
      // initialize tables
      await db.execute(
          'CREATE TABLE series(url TEXT UNIQUE, cover_url TEXT, title TEXT, author TEXT, status VARCHAR, scraper VARCHAR, downloaded BOOLEAN DEFAULT 0, last_read INTEGER, last_chapter INTEGER DEFAULT -1, last_page INTEGER DEFAULT 0, nofchapters INTEGER DEFAULT 0, new_chapters INTEGER DEFAULT -1, favourite TINYINT DEFAULT 0, is_webtoon BOOLEAN DEFAULT 0, asked_if_webtoon BOOLEAN DEFAULT 0)');
      await db.execute(
          'CREATE TABLE chapters(id INTEGER PRIMARY KEY, series_url TEXT, url TEXT, ord INTEGER, title TEXT, download_status TINYINT, read INTEGER)');
    }, onUpgrade: (Database db, int oldVersion, int newVersion) async {
      if (oldVersion <= 1) {
        await db.execute('ALTER TABLE series ADD favourite TINYINT DEFAULT 0');
      }
      if (oldVersion <= 2) {
        await db.execute('ALTER TABLE series ADD is_webtoon BOOLEAN DEFAULT 0');
        await db.execute(
            'ALTER TABLE series ADD asked_if_webtoon BOOLEAN DEFAULT 0');
      }
      if (oldVersion <= 3) {
        await db.execute('ALTER TABLE chapters ADD url TEXT');
      }
      if (oldVersion <= 4) {
        // remove series from now-unsupported scrapers
        List<String> blacklist = [
          'MangaMutiny.org',
          'r2.OnlineManga.xyz',
          'MangaToon.top'
        ];
        for (var scraper in blacklist) {
          await db.delete(
            'series',
            where: 'scraper = ?',
            whereArgs: [scraper],
          );
        }
      }
    });
  }

  static Future<bool> isInDb(String url) async {
    var db = await database;
    var results = await db.query(
      'series',
      where: 'url = ?',
      whereArgs: [url],
    );
    return results.isNotEmpty;
  }

  static Future<Series> loadSeries(String url,
      {bool metadataOnly = false}) async {
    var db = await database;
    var results = await db.query(
      'series',
      where: 'url = ?',
      whereArgs: [url],
    );
    Series series = Series.fromMap(results[0]);

    if (metadataOnly) return series;

    // load chapters
    return loadChapters(series);
  }

  static Future<Series> loadChapters(Series series) async {
    var db = await database;
    var results = await db.query(
      'chapters',
      where: 'series_url = ?',
      whereArgs: [series.url],
      orderBy: 'ord',
    );

    List<Chapter> chapters =
        results.map<Chapter>((e) => Chapter.fromMap(e)).toList();

    if (series.chapters == null) {
      series = series.copyWith(chapters: []);
    }

    for (Chapter chapter in chapters) {
      if (chapter.ord < series.chapters!.length) {
        series.chapters![chapter.ord] = chapter;
      } else {
        series.chapters!.add(chapter);
      }
    }

    return series;
  }

  static Future<List<Series>> loadSeriesList(List<String> urls,
      {bool metadataOnly = true}) async {
    return Future.wait(
        urls.map((url) => loadSeries(url, metadataOnly: metadataOnly)));
  }

  static Future<Series> updateSeriesWithUserData(Series series) async {
    if (!await isInDb(series.url)) return series;
    var loadedSeries = await loadSeries(series.url, metadataOnly: true);
    var newSeries = series.copyWith(
      favourite: loadedSeries.favourite,
      isWebtoon: loadedSeries.isWebtoon,
      askedIfWebtoon: loadedSeries.askedIfWebtoon,
      lastReadData: loadedSeries.lastReadData,
      lastChapter: loadedSeries.lastChapter,
      lastPage: loadedSeries.lastPage,
      nofChapters: loadedSeries.nofChapters,
      newChapters: loadedSeries.newChapters,
    );
    return loadChapters(newSeries);
  }

  static Future<Series> saveSeries(Series series,
      {bool metadataOnly = false, bool noProgressUpdate = false}) async {
    var db = await database;

    // force chapter save if first time saving
    int chaptersInDb = (await db.query('chapters', where: 'series_url = ?', whereArgs: [series.url])).length;
    if(series.chapters != null && chaptersInDb  <= 1) {
      metadataOnly = false;
    }

    if(series.chapters != null) {
      series = series.copyWith(nofChapters: series.chapters!.length);
    }
    if (!noProgressUpdate) {
      series =
          series.copyWith(lastReadData: DateTime.now().millisecondsSinceEpoch, newChapters: series.nofChapters);
    } else {
      int newChapters = (await db.query('series', columns: ['new_chapters'], where: 'url = ?', whereArgs: [series.url])).first['new_chapters'] as int;
      series = series.copyWith(newChapters: newChapters);
    }
    db.insert('series', series.toMap(),
        conflictAlgorithm: ConflictAlgorithm.replace);
    if (metadataOnly) return series;
    return saveChapters(series);
  }

  static Future<Series> saveChapters(Series series) async {
    var db = await database;

    var results = await db.query(
      'chapters',
      where: 'series_url = ?',
      whereArgs: [series.url],
    );

    for (int i = 0; i < series.chapters!.length; ++i) {
      var chapter = series.chapters![i];

      if (results.length > i) {
        // update chapter
        await db.update(
          'chapters',
          chapter.toMap(),
          where: 'series_url = ? AND ord = ?',
          whereArgs: [chapter.seriesUrl, chapter.ord],
          conflictAlgorithm: ConflictAlgorithm.replace,
        );
      } else {
        // insert new chapter
        await db.insert(
          'chapters',
          chapter.toMap(),
        );
      }
    }

    return series;
  }

  static Future<void> saveChapter(Chapter chapter) async {
    var db = await database;

    var res = await db.query(
      'chapters',
      where: 'series_url = ? AND ord = ?',
      whereArgs: [chapter.seriesUrl, chapter.ord],
    );

    if (res.length > 0) {
      // chapter already in database - update
      await db.update(
        'chapters',
        chapter.toMap(),
        where: 'series_url = ? AND ord = ?',
        whereArgs: [chapter.seriesUrl, chapter.ord],
        conflictAlgorithm: ConflictAlgorithm.replace,
      );
    } else {
      // insert new chapter
      await db.insert(
        'chapters',
        chapter.toMap(),
      );
    }
  }

  static Future<Chapter> markChapterRead(Chapter chapter) async {
    chapter = chapter.copyWith(read: true);
    await DBManager.saveChapter(chapter);
    return chapter;
  }

  static Future<void> removeSeries(Series series) async {
    var db = await database;
    await db.delete(
      'chapters',
      where: 'series_url = ?',
      whereArgs: [series.url],
    );
    await db.delete(
      'series',
      where: 'url = ?',
      whereArgs: [series.url],
    );
  }

  // getting groups of series

  static Future<List<Series>> loadHistory() async {
    var db = await database;
    var result = await db.query(
      'series',
      columns: ['url'],
      where: 'last_read > -1',
      orderBy: 'last_read',
    );
    result = result.reversed.toList();
    return loadSeriesList(result.map((e) => e.values.first as String).toList());
  }

  static Future<List<Series>> loadFavourite() async {
    var db = await database;
    var result = await db.query(
      'series',
      where: 'favourite = 1',
      orderBy: 'last_read',
    );
    result = result.reversed.toList();
    return loadSeriesList(result.map((e) => e.values.first as String).toList());
  }

  static Future<List<Series>> loadDownloaded() async {
    var db = await database;
    var result = await db.query(
      'series',
      where: 'downloaded = 1',
    );
    return loadSeriesList(result.map((e) => e.values.first as String).toList());
  }

  // Google Drive stuff

  static Future<String?> backupFileId() async {
    final driveApi = await GoogleAuthClient.signIn();

    var result = await driveApi.files.list(spaces: 'appDataFolder');
    for (var f in result.files!) {
      if (f.name == _dbFilename) return f.id;
    }
    return null;
  }

  static Future<void> backupData() async {
    final driveApi = await GoogleAuthClient.signIn();
    final uploadFile = drive.File();
    final dbFile = File(await dbFilepath());

    uploadFile.parents = ["appDataFolder"];
    uploadFile.name = _dbFilename;
    await driveApi.files.create(
      uploadFile,
      uploadMedia: drive.Media(dbFile.openRead(), dbFile.lengthSync()),
    );
  }

  static Future<bool> loadBackupData() async {
    final driveApi = await GoogleAuthClient.signIn();
    final backupId = await backupFileId();
    if (backupId == null) return false;
    drive.Media downloadFile = await driveApi.files.get(backupId,
        downloadOptions: drive.DownloadOptions.fullMedia) as drive.Media;
    final dbFile = File(await dbFilepath());

    List<int> dataStore = [];
    bool success = false;
    downloadFile.stream.listen((data) {
      dataStore.insertAll(dataStore.length, data);
    }, onDone: () {
      dbFile.writeAsBytes(dataStore);
      success = true;
    }, onError: (error) {
      success = false;
    });

    if (success) await loadDatabase();

    return success;
  }

  /*static Future<void> tempLoad() async {
    var f = File("/storage/emulated/0/PSP/series.db");
    await f.copy(await dbFilepath());
    await loadDatabase();
    logger.w("done");
  }*/

  // integrity checks and other fixing tools

  static Future<void> integrityCheck() async {
    // remove duplicate chapters and verify chapters quantity in every series
    var db = await DBManager.database;
    var series = await db.query('series');

    for (var serie in series) {
      var chapters = await db.query(
        'chapters',
        where: 'series_url = ?',
        whereArgs: [serie['url']],
        orderBy: 'ord',
      );

      int chaptersCount = chapters.length;

      int prevord = -1;
      for (var chapter in chapters) {
        if (chapter['ord'] == prevord) {
          // found duplicate
          db.delete(
            'chapters',
            where: 'id = ?',
            whereArgs: [chapter['id']],
          );
          chaptersCount--;
        }
        prevord = chapter['ord'] as int;
      }

      await db.update(
        'series',
        {'nofchapters': chaptersCount},
        where: 'url = ?',
        whereArgs: [serie['url']],
        conflictAlgorithm: ConflictAlgorithm.replace,
      );
    }
  }
}
