import 'package:http/http.dart' as http;
import 'package:googleapis/drive/v3.dart' as drive;
import 'package:google_sign_in/google_sign_in.dart' as gsignIn;

class GoogleAuthClient extends http.BaseClient {
  final Map<String, String> _headers;

  final http.Client _client = new http.Client();

  GoogleAuthClient(this._headers);

  Future<http.StreamedResponse> send(http.BaseRequest request) {
    return _client.send(request..headers.addAll(_headers));
  }

  static Future<drive.DriveApi> signIn() async {
    final googleSignIn = gsignIn.GoogleSignIn.standard(scopes: [drive.DriveApi.driveAppdataScope]);
    gsignIn.GoogleSignInAccount? account = await googleSignIn.signInSilently();
    if (account == null)
      account = await googleSignIn.signIn();
    final authenticateClient = GoogleAuthClient(await account!.authHeaders);
    return drive.DriveApi(authenticateClient);
  }

  static Future<void> logOut() async {
    final googleSignIn = gsignIn.GoogleSignIn.standard(scopes: [drive.DriveApi.driveAppdataScope]);
    googleSignIn.signOut();
  }
}