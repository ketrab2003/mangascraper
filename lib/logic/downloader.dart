import 'package:scraper/logic/scraper_interfaces.dart';
import 'package:dio/dio.dart';
import 'dart:io';

import 'package:scraper/logic/scraper_manager.dart';
import 'package:scraper/logic/db_manager.dart';

import 'package:connectivity/connectivity.dart';
import 'package:scraper/logic/settings.dart';

import 'package:logger/logger.dart';

var logger = Logger(
  printer: PrettyPrinter(),
);

// TODO: add notification when downloading

/*String getExtension(String path) {
  var res = p.extension(path);
  return res.split("?")[0];
}*/

class Downloader {
  static Future<void> downloadImage(String url, String filepath) async {
    if (FileSystemEntity.isFileSync(filepath)) {
      logger.i("Skipping:\n$url ->\n$filepath");
      return;
    }
    logger.i("Downloading:\n$url ->\n$filepath");

    Dio().download(url, filepath);
  }

  static Future<bool> isConnected() async {
    var connectivityResult = await Connectivity().checkConnectivity();
    if (connectivityResult == ConnectivityResult.none) // no internet
      return false;
    if (connectivityResult == ConnectivityResult.mobile &&
        !(await Settings.getInstance()).getBool('allow-mobile', false)) // mobile data
      return false;
    return true;
  }

  // new chapters check

  static Future<void> checkForNewChapters(String url) async {
    try {
      Series s = await DBManager.loadSeries(url);

      logger.i("Updating ${s.title} : ${s.newChapters}");
      s = await ScraperManager.updateSeriesWithChapters(s);
      if (s.newChapters < s.chapters!.length) {
        logger.i("${s.title} : New chapters ${s.chapters!.length}");
        s = await DBManager.saveSeries(s, noProgressUpdate: true);
      }
    } catch (e) {
      logger.w("Chapter checking error: $e");
    }
  }

  static Future<void> checkAllForNewChapters() async {
    logger.i("Starting new chapter check...");

    // Check internet connection

    var connectivityResult = await (Connectivity().checkConnectivity());
    if (connectivityResult == ConnectivityResult.none) {
      // no internet
      logger.i("No Internet - skipping\n");
      logger.i("Finished session at ${DateTime.now()}\n");
      return;
    }
    if (connectivityResult == ConnectivityResult.mobile &&
        !(await Settings.getInstance()).getBool('allow-mobile', false)) {
      // mobile data
      logger.i("Mobile data is not allowed - skipping\n");
      logger.i("Finished session at ${DateTime.now()}\n");
      return;
    }

    // Check interval

    var intervalDecode = {
      'm': Duration(minutes: 15),
      'h': Duration(hours: 1),
      'd': Duration(days: 1),
      'w': Duration(days: 7),
    };
    var interval =
        intervalDecode[(await Settings.getInstance()).getString('downloaded-interval', 'm')];
    var lastchecked = DateTime.fromMicrosecondsSinceEpoch(
        (await Settings.getInstance()).getInt('downloaded-last', 0));
    var now = DateTime.now();

    logger.i(
        "Now is: $now\nLast: $lastchecked\nInterval: $interval\nAdded interval: ${lastchecked.add(interval!)}");

    if (lastchecked.add(interval).isAfter(now)) {
      logger.i("Too short interval passed - skipping\n");
      logger.i("Finished session at ${DateTime.now()}\n");
      return;
    }

    (await Settings.getInstance()).save('downloaded-last', now.microsecondsSinceEpoch);

    // Start checking for new chapters

    var db = await DBManager.database;

    var series = await db.query(
      'series',
      columns: ['url'],
      where: 'new_chapters > -1 AND status <> ?',
      whereArgs: ['completed'],
    );

    for (var serie in series) {
      // check for needed series
      await checkForNewChapters(serie['url'] as String);
    }

    logger.i("Finished session at ${DateTime.now()}\n");
  }

  static Future<void> tryToBackup() async {
    if (!(await Settings.getInstance()).getBool("drive-enabled", false)) return;

    logger.i("Starting backup...");

    // Check internet connection

    var connectivityResult = await (Connectivity().checkConnectivity());
    if (connectivityResult == ConnectivityResult.none) {
      // no internet
      logger.i("[Backup] No Internet - skipping\n");
      logger.i("[Backup] Finished session at ${DateTime.now()}\n");
      return;
    }
    if (connectivityResult == ConnectivityResult.mobile &&
        !(await Settings.getInstance()).getBool('allow-mobile', false)) {
      // mobile data
      logger.i("[Backup] Mobile data is not allowed - skipping\n");
      logger.i("[Backup] Finished session at ${DateTime.now()}\n");
      return;
    }

    // Check interval
    var lastchecked = DateTime.fromMicrosecondsSinceEpoch(
        (await Settings.getInstance()).getInt('backup-last', 0));
    var now = DateTime.now();

    logger.i("[Backup] Now is: $now\nLast: $lastchecked");


    if (lastchecked.add(Duration(minutes: 5)).isAfter(now)) {
      logger.i("[Backup] Too short interval passed - skipping\n");
      logger.i("[Backup] Finished session at ${DateTime.now()}\n");
      return;
    }

    // Backup data

    await DBManager.backupData();

    (await Settings.getInstance()).save('backup-last', now.microsecondsSinceEpoch);
    logger.i("[Backup] Finished session at ${DateTime.now()}\n");
  }

  // true downloads
  // TODO: implement
}
