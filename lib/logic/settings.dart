import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:shared_preferences/shared_preferences.dart';

final settingsProvider = FutureProvider((ref) async {
  return await Settings.getInstance();
});

class Settings {
  SharedPreferences _instance;

  Settings(this._instance);

  static Future<Settings> getInstance() async {
    var spInstance = await SharedPreferences.getInstance();
    return Settings(spInstance);
  }

  int getInt(String key, int defaultValue) {
    return this._instance.getInt(key) ?? defaultValue;
  }

  String getString(String key, String defaultValue) {
    return this._instance.getString(key) ??
        defaultValue;
  }

  double getDouble(String key, double defaultValue) {
    return this._instance.getDouble(key) ??
        defaultValue;
  }

  bool getBool(String key, bool defaultValue) {
    return this._instance.getBool(key) ?? defaultValue;
  }

  Future<void> save(String key, dynamic value) async {
    if (value is int) {
      await this._instance.setInt(key, value);
    } else if (value is String) {
      await this._instance.setString(key, value);
    } else if (value is double) {
      await this._instance.setDouble(key, value);
    } else if (value is bool) {
      await this._instance.setBool(key, value);
    }
  }
}