// This file provides correct scrapers based on asker's needs

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:scraper/logic/cache_classes.dart';

import 'package:scraper/logic/db_manager.dart';
import 'package:scraper/logic/scraper_interfaces.dart';

// here import new scrapers with uniquie import names <---------------------
import 'package:scraper/scrapers/guya.dart';
import 'package:scraper/scrapers/dex.dart';
import 'package:scraper/scrapers/hub.dart';

import 'package:logger/logger.dart';

var logger = Logger(
  printer: PrettyPrinter(),
);

final seriesProvider = StateNotifierProvider.autoDispose.family<SeriesNotifier, Series, Series>((ref, series) {
  return SeriesNotifier(series);
});

class SeriesNotifier extends StateNotifier<Series> {
  SeriesNotifier(Series series) : super(series);
  void updateState(Series s) => state = s;
}

class ScraperManager {
  static Map<String, IScraper> scrapers = {
    Guya().name(): Guya(),
    Dex().name(): Dex(),
    Hub().name(): Hub(),
  };

  static Future<List<Series>> search(String term, {String? onlyScraper}) async {
    // return search results from all scrapers combined (unless onlyScraper specified)
    List<List<Series>> unshuffled = [];

    if(onlyScraper != "All" && onlyScraper != null){
      return await scrapers[onlyScraper]!.search(term);
    }

    for (String scraper in scrapers.keys) {
      try {
        unshuffled.add(await scrapers[scraper]!.search(term));
      } catch (e) {
        logger.w("Error in $scraper search : $e");
      }
    }

    // shuffle results from different searchers
    List<Series> results = [];
    for (int i = 0; true; ++i) {
      bool finished = true;

      for (var list in unshuffled) {
        if (i < list.length) {
          results.add(list[i]);
          finished = false;
        }
      }

      if (finished) break;
    }

    return results;
  }

  static Future<Series?> getSeries(String url) async {
    for (var scraper in scrapers.values) {
      if (scraper.recognize(url)) {
        return scraper.getSeries(url);
      }
    }
  }

  static Future<Series?> getSeriesWithChapters(String url) async {
    for (var scraper in scrapers.values) {
      if (scraper.recognize(url)) {
        return scraper.getSeriesWithChapters(url);
      }
    }
  }

  static Future<Series> updateSeries(Series series) async {
    var updatedSeries = await scrapers[series.scraper]!.getSeries(series.url);
    return DBManager.updateSeriesWithUserData(updatedSeries);
  }

  static Future<Series> updateSeriesWithChapters(Series series) async {
    var updatedSeries =
        await scrapers[series.scraper]!.getSeriesWithChapters(series.url);
    return DBManager.updateSeriesWithUserData(updatedSeries);
  }

  static Future<Series> addChapters(Series series) async {
    series = await scrapers[series.scraper]!.addChapters(series);
    return series.copyWith(nofChapters: series.chapters!.length);
  }

  static Future<Chapter> addPages(Chapter chapter, String scraper) async {
    return _addImages(await scrapers[scraper]!.addPages(chapter));
  }

  static Chapter _addImages(Chapter chapter) {
    List<ImageProvider> images = chapter.pages!
        .map<ImageProvider>((image) => CachedNetworkImageProvider(
              image,
              cacheManager: SeriesCacheManager(chapter.seriesUrl).instance,
            ))
        .toList();

    return chapter.copyWith(images: images);
  }

  static Future<void> precachePages(BuildContext context, Chapter chapter) async {
    try {
      for (var img in chapter.images!) {
        await precacheImage(img, context);
      }
    } catch (e) {}
  }
}