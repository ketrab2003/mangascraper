import 'dart:io';
import 'package:path/path.dart' as p;

import 'package:flutter/material.dart';

import 'package:permission_handler/permission_handler.dart';
import 'package:dio/dio.dart';
import 'package:open_file/open_file.dart';
import 'package:package_info/package_info.dart';
import 'package:path_provider/path_provider.dart';

class UpdateChecker {
  static String installerUrl =
      "https://gitlab.com/api/v4/projects/20268621/repository/files/app-release%2Eapk/raw?ref=master";
  static String versionUrl =
      "https://gitlab.com/api/v4/projects/20268621/repository/files/VERSION/raw?ref=master";

  static Future<String> get localInstallerPath async {
    String path = (await getExternalStorageDirectory())!.path;
    return "$path/MangaScraper/installer.apk";
  }

  static Future<String> get newestVersion async {
    return (await Dio().get(versionUrl)).data.toString();
  }

  static Future<String> get activeVersion async {
    return (await PackageInfo.fromPlatform()).version;
  }

  static Future<bool> get isUpdate async {
    String newVersion;
    try {
      newVersion = await newestVersion;
    } catch (e) {
      return false;
    }

    if (await activeVersion != newVersion && RegExp(r'^\d+\.\d+\.\d+$').hasMatch(newVersion)) {
      return true;
    }
    return false;
  }

  static Future<void> requestPermission() async {
    while (!await Permission.storage.isGranted) {
      Permission.storage.request();
      if (await Permission.storage.isPermanentlyDenied) {
        openAppSettings();
      }
    }
  }

  static Future<void> removeOldInstaller() async {
    if (FileSystemEntity.isFileSync(await localInstallerPath)) {
      File(await localInstallerPath).deleteSync(recursive: true);
    }
  }

  static Future<String> downloadInstaller(Function(int, int) onProgress) async {
    String path = await localInstallerPath;
    try {
      Directory(p.dirname(path)).createSync(recursive: true);
    } catch (e) {}

    await Dio().download(installerUrl, path, onReceiveProgress: onProgress);
    return path;
  }

  static Future<void> downloadAndInstall(Function(int, int) onProgress) async {
    await OpenFile.open(
      await downloadInstaller(onProgress),
    );
  }

  static Future<void> showUpdateDialog(BuildContext context) async {
    String a = await activeVersion;
    String b = await newestVersion;

    await showDialog(
      context: context,
      barrierDismissible: false,
      builder: (context) => UpdateAlert(
        currentVersion: a,
        newestVersion: b,
      ),
    );
  }
}

class UpdateAlert extends StatefulWidget {
  final String currentVersion;
  final String newestVersion;

  UpdateAlert({required this.currentVersion, required this.newestVersion});

  @override
  _UpdateAlertState createState() => _UpdateAlertState();
}

class _UpdateAlertState extends State<UpdateAlert> {
  double progress = 0;
  bool canCancel = true;

  void onProgress(int downloaded, int total) {
    setState(() {
      progress = downloaded / total;
    });
  }

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      title: Text('New app version'),
      content: SingleChildScrollView(
        child: ListBody(
          children: <Widget>[
            Text('Your app version: ${widget.currentVersion}'),
            Text('Update version: ${widget.newestVersion}'),
            Text("(It's recommended to update ASAP)"),
            Text(""),
            LinearProgressIndicator(
              value: progress,
            ),
          ],
        ),
      ),
      actions: <Widget>[
        TextButton(
          child: Text('Cancel'),
          onPressed: (canCancel ? () => Navigator.of(context).pop() : null),
        ),
        TextButton(
          child: Text(
            'Update',
            style: TextStyle(color: Colors.green),
          ),
          onPressed: () async {
            setState(() {
              canCancel = false;
            });
            await UpdateChecker.downloadAndInstall(onProgress);
            Navigator.of(context).pop();
          },
        ),
      ],
    );
  }
}
