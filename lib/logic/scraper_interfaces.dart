import 'dart:convert';

import 'package:flutter/foundation.dart';

/* This file is template for creating scrapers
 * Every new scraper needs to inherit from interfaces defined in this file
 * and provide own following parameters and methods to them:
 * - Scraper.search() - returns Future<List<Scraper>> object filled with series found in scraped page's search engine
 *                      found series needs to have provided: url, coverUrl and title
 * - Series.getData() - scraps data from internet: title, author, status (ongoing, etc..) and chapters (title and url)
 * - Series.scraper() - returns constant String with the name of created scraper (for UI and identification purposes)
 * - Chapter.getData() - scraps data from internet: title and images
 * To solve some inaccuracies check how other scrapers in this project are written
 */

import 'package:flutter/material.dart';

import 'package:sqflite/sqflite.dart';
import 'package:scraper/logic/cache_classes.dart';
import 'package:cached_network_image/cached_network_image.dart';

import 'package:scraper/logic/db_manager.dart';

import 'package:logger/logger.dart';

var logger = Logger(
  printer: PrettyPrinter(),
);

abstract class IScraper {
  bool recognize(String url);
  String name();
  Future<List<Series>> search(String term);
  Future<Series> getSeries(String url);
  Future<Series> addChapters(Series series);
  Future<Chapter> addPages(Chapter chapter);

  Future<Series> getSeriesWithChapters(String seriesUrl) async {
    Series series = await this.getSeries(seriesUrl);
    series = await this.addChapters(series);
    return series;
  }
}

class Series {
  // identifier
  final String url;
  // --- metadata ---
  final String title;
  final String author;
  final String status;
  final String coverUrl;
  final String scraper;
  final int nofChapters;
  // --- user settings ---
  final bool favourite;
  final bool isWebtoon;
  final bool askedIfWebtoon;
  // --- user progress ---
  final int lastReadData;
  final int lastChapter;
  final int lastPage;

  final int newChapters;
  // --- data ---
  final List<Chapter>? chapters;

  Series({
    required this.url,
    required this.title,
    required this.author,
    required this.status,
    required this.coverUrl,
    required this.scraper,
    this.nofChapters = 0,
    this.favourite = false,
    this.isWebtoon = false,
    this.askedIfWebtoon = false,
    this.lastReadData = -1,
    this.lastChapter = -1,
    this.lastPage = 0,
    this.newChapters = -1,
    this.chapters,
  });

  ImageProvider get cover {
    // return series cover image
    ImageProvider? p = CoverCacheManager.getImage(this.coverUrl);
    if (p == null) {
      p = CachedNetworkImageProvider(
        this.coverUrl,
        cacheManager: CoverCacheManager.instance,
      );
      CoverCacheManager.setImage(this.coverUrl, p);
    }
    return p;
  }

  Series copyWith({
    String? url,
    String? title,
    String? author,
    String? status,
    String? coverUrl,
    String? scraper,
    int? nofChapters,
    bool? favourite,
    bool? isWebtoon,
    bool? askedIfWebtoon,
    int? lastReadData,
    int? lastChapter,
    int? lastPage,
    int? newChapters,
    List<Chapter>? chapters,
  }) {
    return Series(
      url: url ?? this.url,
      title: title ?? this.title,
      author: author ?? this.author,
      status: status ?? this.status,
      coverUrl: coverUrl ?? this.coverUrl,
      scraper: scraper ?? this.scraper,
      nofChapters: nofChapters ?? this.nofChapters,
      favourite: favourite ?? this.favourite,
      isWebtoon: isWebtoon ?? this.isWebtoon,
      askedIfWebtoon: askedIfWebtoon ?? this.askedIfWebtoon,
      lastReadData: lastReadData ?? this.lastReadData,
      lastChapter: lastChapter ?? this.lastChapter,
      lastPage: lastPage ?? this.lastPage,
      newChapters: newChapters ?? this.newChapters,
      chapters: chapters ?? this.chapters,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'url': url,
      'title': title,
      'author': author,
      'status': status,
      'cover_url': coverUrl,
      'scraper': scraper,
      'nofchapters': nofChapters,
      'favourite': (favourite ? 1 : 0),
      'is_webtoon': (isWebtoon ? 1 : 0),
      'asked_if_webtoon': (askedIfWebtoon ? 1 : 0),
      'last_read': lastReadData,
      'last_chapter': lastChapter,
      'last_page': lastPage,
      'new_chapters': newChapters,
    };
  }

  factory Series.fromMap(Map<String, dynamic> map) {
    return Series(
      url: map['url'],
      title: map['title'],
      author: map['author'],
      status: map['status'],
      coverUrl: map['cover_url'],
      scraper: map['scraper'],
      nofChapters: map['nofchapters'],
      favourite: map['favourite'] == 1,
      isWebtoon: map['is_webtoon'] == 1,
      askedIfWebtoon: map['asked_if_webtoon'] == 1,
      lastReadData: map['last_read'],
      lastChapter: map['last_chapter'],
      lastPage: map['last_page'],
      newChapters: map['new_chapters'],
    );
  }

  String toJson() => json.encode(toMap());

  factory Series.fromJson(String source) => Series.fromMap(json.decode(source));

  @override
  String toString() {
    return 'Series(url: $url, title: $title, author: $author, status: $status, coverUrl: $coverUrl, scraper: $scraper, nofChapters: $nofChapters, favourite: $favourite, isWebtoon: $isWebtoon, askedIfWebtoon: $askedIfWebtoon, lastReadData: $lastReadData, lastChapter: $lastChapter, lastPage: $lastPage, newChapters: $newChapters, chapters: $chapters)';
  }

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;

    return other is Series &&
        other.url == url &&
        other.title == title &&
        other.author == author &&
        other.status == status &&
        other.coverUrl == coverUrl &&
        other.scraper == scraper &&
        other.nofChapters == nofChapters &&
        other.favourite == favourite &&
        other.isWebtoon == isWebtoon &&
        other.askedIfWebtoon == askedIfWebtoon &&
        other.lastReadData == lastReadData &&
        other.lastChapter == lastChapter &&
        other.lastPage == lastPage &&
        other.newChapters == newChapters &&
        listEquals(other.chapters, chapters);
  }

  @override
  int get hashCode {
    return url.hashCode ^
        title.hashCode ^
        author.hashCode ^
        status.hashCode ^
        coverUrl.hashCode ^
        scraper.hashCode ^
        nofChapters.hashCode ^
        favourite.hashCode ^
        isWebtoon.hashCode ^
        askedIfWebtoon.hashCode ^
        lastReadData.hashCode ^
        lastChapter.hashCode ^
        lastPage.hashCode ^
        newChapters.hashCode ^
        chapters.hashCode;
  }
}

class Chapter {
  // --- identifiers ---
  final String url;
  final String seriesUrl;
  // --- metadata ---
  final String title;
  final int ord;
  // --- user progress ---
  final bool read;
  // --- data ---
  final List<String>? pages;
  final List<ImageProvider>? images;

  Chapter({
    required this.url,
    required this.seriesUrl,
    required this.title,
    required this.ord,
    this.read = false,
    this.pages,
    this.images,
  });

  Chapter copyWith({
    String? url,
    String? seriesUrl,
    String? title,
    int? ord,
    bool? read,
    List<String>? pages,
    List<ImageProvider>? images,
  }) {
    return Chapter(
      url: url ?? this.url,
      seriesUrl: seriesUrl ?? this.seriesUrl,
      title: title ?? this.title,
      ord: ord ?? this.ord,
      read: read ?? this.read,
      pages: pages ?? this.pages,
      images: images ?? this.images,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'url': url,
      'series_url': seriesUrl,
      'title': title,
      'ord': ord,
      'read': (read ? 1 : 0),
    };
  }

  factory Chapter.fromMap(Map<String, dynamic> map) {
    return Chapter(
      url: map['url'] ?? '',
      seriesUrl: map['series_url'] ?? '',
      title: map['title'] ?? '',
      ord: map['ord']?.toInt() ?? 0,
      read: (map['read'] == 1),
    );
  }

  String toJson() => json.encode(toMap());

  factory Chapter.fromJson(String source) =>
      Chapter.fromMap(json.decode(source));

  @override
  String toString() {
    return 'Chapter(url: $url, seriesUrl: $seriesUrl, title: $title, ord: $ord, read: $read, pages: $pages, images: $images)';
  }

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;
  
    return other is Chapter &&
      other.url == url &&
      other.seriesUrl == seriesUrl &&
      other.title == title &&
      other.ord == ord &&
      other.read == read &&
      listEquals(other.pages, pages) &&
      listEquals(other.images, images);
  }

  @override
  int get hashCode {
    return url.hashCode ^
      seriesUrl.hashCode ^
      title.hashCode ^
      ord.hashCode ^
      read.hashCode ^
      pages.hashCode ^
      images.hashCode;
  }
}