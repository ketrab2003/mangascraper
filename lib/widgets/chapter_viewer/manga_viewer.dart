import 'package:flutter/material.dart';
import 'package:scraper/logic/db_manager.dart';
import 'package:photo_view/photo_view.dart';
import 'package:photo_view/photo_view_gallery.dart';

import 'package:scraper/logic/scraper_interfaces.dart';
import 'package:scraper/widgets/chapter_viewer/chapter_viewer.dart';

class MangaViewer extends StatefulWidget {
  final Series series;
  final int chapterIndex;
  final int pageIndex;
  final Function? refresh;
  final PageController pageController;
  final Function updatePageCounter;

  MangaViewer(
      {Key? key,
      required this.series,
      required this.chapterIndex,
      this.pageIndex = 0,
      this.refresh,
      required this.pageController,
      required this.updatePageCounter})
      : super(key: key);

  @override
  _MangaViewerState createState() => _MangaViewerState();
}

class _MangaViewerState extends State<MangaViewer> {

  void onPageChanged(int currentPage) async {

    // move to the previous chapter
    if (widget.chapterIndex > 0 &&
        widget.series.chapters![widget.chapterIndex - 1].images!.length > 0 &&
        currentPage == 0) {
      this.moveToPreviousChapter();
      return;
    }

    // move to the next chapter
    if (widget.chapterIndex != widget.series.chapters!.length - 1 &&
        currentPage ==
            widget.series.chapters![widget.chapterIndex].images!.length +
                (widget.chapterIndex == 0 ||
                        widget.series.chapters![widget.chapterIndex - 1].images!
                                .length <
                            1
                    ? 0
                    : 1)) {
      this.moveToNextChapter();
      return;
    }

    // if the last chapter, mark as read at the last page
    if (widget.chapterIndex == widget.series.chapters!.length - 1 &&
        currentPage ==
            widget.series.chapters![widget.chapterIndex].images!.length +
                (widget.chapterIndex == 0 ||
                        widget.series.chapters![widget.chapterIndex - 1].images!
                                .length <
                            1
                    ? -1
                    : 0)) {
      widget.series.chapters![widget.chapterIndex] =
          await DBManager.markChapterRead(
              widget.series.chapters![widget.chapterIndex]);
    }

    DBManager.saveSeries(
        widget.series.copyWith(
          lastChapter: widget.chapterIndex,
          lastPage: currentPage - (widget.chapterIndex != 0 ? 1 : 0),
        ),
        metadataOnly: true);

    if (widget.refresh != null) widget.refresh!();
    widget.updatePageCounter(currentPage);
  }

  void moveToNextChapter() async {
    widget.series.chapters![widget.chapterIndex] =
          await DBManager.markChapterRead(
              widget.series.chapters![widget.chapterIndex]);
      DBManager.saveSeries(
          widget.series.copyWith(lastChapter: widget.chapterIndex + 1),
          metadataOnly: true);
      if (widget.refresh != null) widget.refresh!();
      Navigator.of(context).pushReplacement(PageRouteBuilder(
        pageBuilder: (_, __, ___) => ChapterViewer(
          series: widget.series,
          chapterIndex: widget.chapterIndex + 1,
          refresh: widget.refresh,
        ),
      ));
  }

  void moveToPreviousChapter() async {
    Navigator.of(context).pushReplacement(PageRouteBuilder(
        pageBuilder: (_, __, ___) => ChapterViewer(
          series: widget.series,
          chapterIndex: widget.chapterIndex - 1,
          pageIndex:
              widget.series.chapters![widget.chapterIndex - 1].images!.length -
                  1,
          refresh: widget.refresh,
        ),
      ));
  }

  @override
  Widget build(BuildContext context) {
    return PhotoViewGallery(
      pageOptions: (widget.chapterIndex == 0 // check if previous chapter not exists
                  ||
                  widget.series.chapters![widget.chapterIndex - 1].images!
                          .length <
                      1 // check if loaded images from previous chapter
              ? <PhotoViewGalleryPageOptions>[]
              : <PhotoViewGalleryPageOptions>[
                  PhotoViewGalleryPageOptions(
                    imageProvider: widget.series
                        .chapters![widget.chapterIndex - 1].images!.reversed
                        .toList()[0],
                    heroAttributes: PhotoViewHeroAttributes(
                        tag: widget.series.chapters![widget.chapterIndex - 1]
                            .images!.reversed
                            .toList()[0]),
                  ),
                ])
          // load proper pages
          +
          widget.series.chapters![widget.chapterIndex].images!
              .map<PhotoViewGalleryPageOptions>(
                (image) => PhotoViewGalleryPageOptions(
                  imageProvider: image,
                  heroAttributes: PhotoViewHeroAttributes(tag: image),
                ),
              )
              .toList()
          // load first page from next chapter if possible
          +
          (widget.chapterIndex == widget.series.chapters!.length - 1 ||
                  widget.series.chapters![widget.chapterIndex + 1].images!
                          .length <
                      1
              ? <PhotoViewGalleryPageOptions>[]
              : <PhotoViewGalleryPageOptions>[
                  PhotoViewGalleryPageOptions(
                    imageProvider: widget
                        .series.chapters![widget.chapterIndex + 1].images![0],
                    heroAttributes: PhotoViewHeroAttributes(
                        tag: widget.series.chapters![widget.chapterIndex + 1]
                            .images![0]),
                  ),
                ]),
      reverse: true,
      pageController: widget.pageController,
      onPageChanged: (currentPage) async {
        this.onPageChanged(currentPage);
      },
    );
  }
}
