import 'package:flutter/material.dart';
import 'package:scraper/logic/db_manager.dart';
import 'package:scraper/logic/scraper_manager.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:flutter/services.dart';

import 'package:scraper/logic/scraper_interfaces.dart';

import 'package:scraper/widgets/chapter_viewer/manga_viewer.dart';
import 'package:scraper/widgets/chapter_viewer/webtoon_viewer.dart';

class ChapterViewer extends StatefulWidget {
  final Series series;
  final int chapterIndex;
  final int pageIndex;
  final Function? refresh;

  ChapterViewer(
      {Key? key,
      required this.series,
      required this.chapterIndex,
      this.pageIndex = 0,
      this.refresh})
      : super(key: key);

  @override
  _ChapterViewerState createState() => _ChapterViewerState();
}

class _ChapterViewerState extends State<ChapterViewer>
    with SingleTickerProviderStateMixin {
  PageController _pageController = PageController();
  AnimationController? _animController;

  late Series series;

  @override
  void initState() {
    super.initState();

    pageNum = widget.pageIndex + (widget.chapterIndex != 0 ? 1 : 0);
    series = widget.series;

    _animController = AnimationController(
      vsync: this,
      duration: Duration(microseconds: 300),
    );

    SystemChrome.setEnabledSystemUIMode(
      SystemUiMode.immersive, overlays: SystemUiOverlay.values);
    series = series.copyWith(
        lastChapter: widget.chapterIndex, lastPage: widget.pageIndex);
    DBManager.saveSeries(series, metadataOnly: true);
  }

  @override
  void dispose() {
    _pageController.dispose();
    _animController!.dispose();

    super.dispose();
  }

  Future<bool> getData() async {
    if (series.chapters == null ||
        series.chapters!.length <= widget.chapterIndex) {
      series = await ScraperManager.addChapters(series);
    }

    if (widget.chapterIndex > 0 && series.chapters![widget.chapterIndex - 1].pages == null) {
      series.chapters![widget.chapterIndex - 1] = await ScraperManager.addPages(
          series.chapters![widget.chapterIndex - 1], series.scraper);
    }

    if (series.chapters![widget.chapterIndex].pages == null) {
      series.chapters![widget.chapterIndex] = await ScraperManager.addPages(
          series.chapters![widget.chapterIndex], series.scraper);
    }
    ScraperManager.precachePages(
          context, series.chapters![widget.chapterIndex]);
    if (series.chapters![widget.chapterIndex].pages == null) {
      Navigator.pop(context);
    }

    if (widget.chapterIndex != series.chapters!.length - 1 && series.chapters![widget.chapterIndex + 1].pages== null) {
      series.chapters![widget.chapterIndex + 1] = await ScraperManager.addPages(
          series.chapters![widget.chapterIndex + 1], series.scraper);
    }
    _pageController = PageController(
      initialPage: (widget.chapterIndex == 0 ||
                  series.chapters![widget.chapterIndex - 1].pages!.length < 1
              ? 0
              : 1) +
          widget.pageIndex,
      keepPage: true,
    );

    return true;
  }

  int? pageNum;

  void updatePageCounter(int val) {
    setState(() {
      pageNum = val;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      extendBody: true,
      bottomNavigationBar: Opacity(
          opacity: 0,
          child: BottomNavigationBar(
            items: [
              BottomNavigationBarItem(
                icon: Icon(Icons.mic_none),
                label: "",
              ),
              BottomNavigationBarItem(
                icon: Icon(Icons.mic_none),
                label: "",
              ),
            ],
            backgroundColor: Colors.green,
          )),
      body: FutureBuilder<bool>(
        future: getData(),
        initialData: false,
        builder: (_, snapshot) {
          if(snapshot.hasError) {
            return Center(
              child: Text('Something went wrong X('),
            );
          }
          if (!snapshot.hasData || !snapshot.data!)
            return Center(
              // show loading screen
              child: SpinKitCubeGrid(
                color: Colors.blue[400],
                size: 40.0,
              ),
            );

          // show proper pages
          return AnimatedBuilder(
            animation: _animController!,
            builder: (context, child) => Stack(
              children: <Widget>[
                GestureDetector(
                    onTap: () {
                      if (_animController!.isCompleted) {
                        _animController!.reverse();
                      } else {
                        _animController!.forward();
                      }
                    },
                    child: (series.isWebtoon
                        ? WebtoonViewer(
                            series: series,
                            chapterIndex: widget.chapterIndex,
                            pageIndex: widget.pageIndex,
                            refresh: widget.refresh,
                            pageController: _pageController,
                            updatePageCounter: updatePageCounter,
                          )
                        : MangaViewer(
                            series: series,
                            chapterIndex: widget.chapterIndex,
                            pageIndex: widget.pageIndex,
                            refresh: widget.refresh,
                            pageController: _pageController,
                            updatePageCounter: updatePageCounter,
                          ))),
                Transform.translate(
                  offset: Offset(0, -_animController!.value * 65),
                  child: Container(
                    height: 65.0,
                    child: AppBar(
                      title: Text(series.chapters![widget.chapterIndex].title),
                      leading: BackButton(
                        onPressed: () {
                          SystemChrome.setEnabledSystemUIMode(
                            SystemUiMode.edgeToEdge, overlays: SystemUiOverlay.values);
                          // SystemChrome.setEnabledSystemUIOverlays(
                          //     SystemUiOverlay.values);
                          Navigator.pop(context);
                        },
                      ),
                      backgroundColor: Colors.grey[800]!.withOpacity(0.4),
                      actions: <Widget>[
                        Padding(
                            padding: EdgeInsets.only(top: 12.0, right: 5.0),
                            child: Text(
                                "${pageNum! + (widget.chapterIndex != 0 ? 0 : 1)}/${series.chapters![widget.chapterIndex].pages!.length}"))
                      ],
                    ),
                  ),
                ),
              ],
            ),
          );
        },
      ),
    );
  }
}