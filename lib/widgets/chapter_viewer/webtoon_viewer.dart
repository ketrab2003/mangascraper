import 'package:flutter/material.dart';
import 'package:scraper/logic/db_manager.dart';

import 'package:scraper/logic/scraper_interfaces.dart';
import 'package:scraper/widgets/chapter_viewer/chapter_viewer.dart';

class WebtoonViewer extends StatefulWidget {
  final Series series;
  final int chapterIndex;
  final int pageIndex;
  final Function? refresh;
  final PageController pageController;
  final Function updatePageCounter;

  WebtoonViewer(
      {Key? key,
      required this.series,
      required this.chapterIndex,
      this.pageIndex = 0,
      this.refresh,
      required this.pageController,
      required this.updatePageCounter})
      : super(key: key);

  @override
  _WebtoonViewerState createState() => _WebtoonViewerState();
}

class _WebtoonViewerState extends State<WebtoonViewer> {
  bool snapping = false;
  ScrollController? _scrollController;
  List<GlobalKey>? imageKeys;
  List? images;

  @override
  void initState() {
    super.initState();

    _scrollController = ScrollController(
        initialScrollOffset: (widget.chapterIndex > 0 ? 50 : 0));

    images = widget.series.chapters![widget.chapterIndex].images;
    imageKeys = List.generate(images!.length, (_) => GlobalKey());
  }

  @override
  void dispose() {
    _scrollController!.dispose();

    super.dispose();
  }

  void jumpToPage(int index) {
    // TODO: try to fix it by saving offset
    var keyContext = imageKeys![0].currentContext;
    var box = keyContext!.findRenderObject() as RenderBox;
    var pos = box.localToGlobal(Offset.zero);
    final delta = (widget.chapterIndex > 0 ? 50 : 0) - pos.dy;

    keyContext = imageKeys![index].currentContext;
    box = keyContext!.findRenderObject() as RenderBox;
    pos = box.localToGlobal(Offset.zero);

    _scrollController!.jumpTo(delta + pos.dy);
  }

  int? _lastReportedPage;
  void onPageChanged(int currentPage) async {
    // if the last chapter, mark as read at the last page
    if (widget.chapterIndex == widget.series.chapters!.length - 1 &&
        currentPage ==
            widget.series.chapters![widget.chapterIndex].images!.length - 1) {
      widget.series.chapters![widget.chapterIndex] =
          await DBManager.markChapterRead(
              widget.series.chapters![widget.chapterIndex]);
    }

    DBManager.saveSeries(
        widget.series.copyWith(
          lastChapter: widget.chapterIndex,
          lastPage: currentPage,
        ),
        metadataOnly: true);

    if (widget.refresh != null) widget.refresh!();
    widget.updatePageCounter(currentPage + 1);
  }

  void moveToNextChapter() async {

    // save progress
    widget.series.chapters![widget.chapterIndex] =
        await DBManager.markChapterRead(
            widget.series.chapters![widget.chapterIndex]);
    DBManager.saveSeries(
        widget.series.copyWith(
          lastChapter: widget.chapterIndex + 1,
          lastPage: widget.chapterIndex,
        ),
        metadataOnly: true);

    if (widget.refresh != null) widget.refresh!();

    Navigator.of(context).pushReplacement(PageRouteBuilder(
      pageBuilder: (_, __, ___) => ChapterViewer(
        series: widget.series,
        chapterIndex: widget.chapterIndex + 1,
        refresh: widget.refresh,
      ),
    ));
  }

  void moveToPreviousChapter() async {
    Navigator.of(context).pushReplacement(PageRouteBuilder(
      pageBuilder: (_, __, ___) => ChapterViewer(
        series: widget.series,
        chapterIndex: widget.chapterIndex - 1,
        pageIndex:
            widget.series.chapters![widget.chapterIndex - 1].images!.length - 1,
        refresh: widget.refresh,
      ),
    ));
  }

  @override
  Widget build(BuildContext context) {
    return NotificationListener<ScrollNotification>(
      onNotification: (ScrollNotification notification) {
        if (notification.depth == 0 &&
            notification is ScrollUpdateNotification) {
          //print(notification.metrics.pixels);
          //final PageMetrics metrics = notification.metrics as PageMetrics;
          //final int currentPage = metrics.page.round();

          int currentPage;
          if (notification.metrics.atEdge && notification.metrics.pixels > 0) {
            currentPage = imageKeys!.length - 1;
          } else {
            for (currentPage = 0;
                currentPage < imageKeys!.length;
                ++currentPage) {
              final keyContext = imageKeys![currentPage].currentContext;
              final box = keyContext!.findRenderObject() as RenderBox;
              final pos = box.localToGlobal(Offset.zero);
              if (pos.dy > 0) break;
            }
            currentPage--;
          }

          if (currentPage < 0) currentPage++;

          if (currentPage != _lastReportedPage) {
            _lastReportedPage = currentPage;
            this.onPageChanged(currentPage);
          }
        }
        return false;
      },
      child: SingleChildScrollView(
        controller: _scrollController,
        //physics: physics,
        //restorationId: widget.restorationId,
        child: Column(
          children: [
            if (widget.chapterIndex > 0)
              Row(mainAxisAlignment: MainAxisAlignment.start, children: [
                TextButton.icon(
                  onPressed: this.moveToPreviousChapter,
                  icon: Icon(Icons.skip_previous, size: 50, color: Colors.grey[400]),
                  label: Text(
                    'Previous Chapter',
                    style: TextStyle(fontSize: 25, color: Colors.grey[400]),
                  ),
                )
              ]),
            ...List.generate(
                images!.length,
                (i) => Image(
                      key: imageKeys![i],
                      image: images![i],
                      width: double.infinity,
                      fit: BoxFit.contain,
                    )).toList(),
            if (widget.chapterIndex != widget.series.chapters!.length - 1) ...[
              Row(mainAxisAlignment: MainAxisAlignment.end, children: [
                TextButton.icon(
                  onPressed: this.moveToNextChapter,
                  icon: Icon(Icons.skip_next, size: 50, color: Colors.grey[400]),
                  label: Text(
                    'Next Chapter',
                    style: TextStyle(fontSize: 25, color: Colors.grey[400]),
                  ),
                )
              ]),
              SizedBox(height: 50),
            ]
          ],
        ),
      ),
    );
  }
}
