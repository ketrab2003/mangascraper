import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

import 'package:scraper/logic/scraper_manager.dart';
import 'package:scraper/widgets/search/search_state.dart';
import 'package:scraper/widgets/series_grid.dart';

final searcherProvider = StateNotifierProvider<Searcher, SearchState>((ref) {
  return Searcher();
});

class Searcher extends StateNotifier<SearchState> {
  Searcher() : super(SearchState());

  void search() async {
    if (state.term.isEmpty) return;
    state = state.copyWith(loaded: false);
    state = state.copyWith(
        results: await ScraperManager.search(state.term,
            onlyScraper: state.chosenScraper),
        loaded: true);
  }

  void changeTerm(String term) async {
    state = state.copyWith(term: term);
    this.search();
  }

  void changeScraper(String? scraper) async {
    state = state.copyWith(chosenScraper: scraper);
    this.search();
  }
}

class SearchPage extends ConsumerWidget {
  const SearchPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final searcher = ref.watch(searcherProvider);
    final searcherNotifier = ref.watch(searcherProvider.notifier);

    return Scaffold(
      backgroundColor: Colors.grey[900],
      appBar: AppBar(
        backgroundColor: Colors.grey[850],
        title: TextFormField(
          initialValue: searcher.term,
          decoration: InputDecoration(
              icon: (searcher.loaded
                  ? Icon(Icons.search)
                  : SizedBox(
                      height: 24.0,
                      width: 24.0,
                      child: CircularProgressIndicator())),
              border: InputBorder.none,
              hintText: "Enter a search term"),
          style: TextStyle(
            color: Colors.grey[200],
          ),
          onFieldSubmitted: (term) => searcherNotifier.changeTerm(term),
        ),
        actions: <Widget>[
          ScraperChooser(),
        ],
      ),
      body: (searcher.results == null
          ? Container()
          : (searcher.results!.isEmpty
              ? Center(child: Text('No results'))
              : SeriesGrid.generateGrid(searcher.results!, false, false))),
    );
  }
}

class ScraperChooser extends ConsumerWidget {
  const ScraperChooser({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final searcher = ref.watch(searcherProvider);
    final searcherNotifier = ref.watch(searcherProvider.notifier);

    return DropdownButton<String>(
      value: searcher.chosenScraper,
      icon: Icon(Icons.arrow_downward),
      elevation: 5,
      style: TextStyle(
        color: Colors.grey[500],
      ),
      onChanged: (value) {
        searcherNotifier.changeScraper(value);
      },
      items: (<String>['All'] + ScraperManager.scrapers.keys.toList())
          .map<DropdownMenuItem<String>>((String value) {
        return DropdownMenuItem<String>(
          value: value,
          child: Text(
            value,
            style: TextStyle(color: Colors.grey[500]),
          ),
        );
      }).toList(),
    );
  }
}
