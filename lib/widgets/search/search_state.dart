import 'package:flutter/foundation.dart';

import 'package:scraper/logic/scraper_interfaces.dart';

class SearchState {
  final String term;
  final String? chosenScraper;
  final bool loaded;
  final List<Series>? results;

  SearchState({
    this.term = '',
    this.chosenScraper = 'All',
    this.loaded = true,
    this.results,
  });

  SearchState copyWith({
    String? term,
    String? chosenScraper,
    bool? loaded,
    List<Series>? results,
  }) {
    return SearchState(
      term: term ?? this.term,
      chosenScraper: chosenScraper ?? this.chosenScraper,
      loaded: loaded ?? this.loaded,
      results: results ?? this.results,
    );
  }

  @override
  String toString() {
    return 'SearchState(term: $term, chosenScraper: $chosenScraper, loaded: $loaded, results: $results)';
  }

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;
  
    return other is SearchState &&
      other.term == term &&
      other.chosenScraper == chosenScraper &&
      other.loaded == loaded &&
      listEquals(other.results, results);
  }

  @override
  int get hashCode {
    return term.hashCode ^
      chosenScraper.hashCode ^
      loaded.hashCode ^
      results.hashCode;
  }
}
