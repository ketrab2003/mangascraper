import 'package:flutter/material.dart';

import 'package:scraper/logic/db_manager.dart';
import 'package:scraper/widgets/series_list.dart';

class FavouritesPage extends StatefulWidget {
  @override
  _FavouritesPageState createState() => _FavouritesPageState();
}

class _FavouritesPageState extends State<FavouritesPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.grey[900],
      appBar: AppBar(
        backgroundColor: Colors.grey[850],
        title: Text("Favourites"),
      ),
      body: SeriesList(
        future: DBManager.loadFavourite(),
        onNoResultsMessage: "No favourites\nAdd some entries ;)",
        refresher: refresher,
        cacheKey: "favourites",
      ),
    );
  }

  void refresher() {
    setState(() {});
  }
}
