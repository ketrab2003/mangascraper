import 'package:flutter/material.dart';

class CustomNavigationBar extends StatelessWidget {
  final int index;
  final PageController controller;

  CustomNavigationBar({required this.index, required this.controller});

  @override
  Widget build(BuildContext context) {
    return BottomNavigationBar(
        items: const <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            icon: Icon(Icons.search),
            label: 'Search',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.access_time),
            label: 'Last read',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.favorite),
            label: 'Favourites',
          ),
          /*BottomNavigationBarItem(
            icon: Icon(Icons.archive),
            label: 'Downloaded',
          ),*/
          /*BottomNavigationBarItem(
            icon: Icon(Icons.collections_bookmark),
            label: 'Subscriptions',
          ),*/
          BottomNavigationBarItem(
            icon: Icon(Icons.settings),
            label: 'Settings',
          ),
        ],
        currentIndex: index,
        selectedItemColor: Colors.grey[400],
        unselectedItemColor: Colors.grey[700],
        onTap: (toIndex) {
          if(index == toIndex)
            return; // ignore going to the same page
          controller.animateToPage(toIndex, duration: Duration(milliseconds: 500), curve: Curves.ease);
        },
      );
  }
}