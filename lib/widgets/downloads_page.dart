// import 'package:flutter/material.dart';

// import 'package:scraper/logic/db_manager.dart';
// import 'package:scraper/widgets/series_grid.dart';

// class DownloadsPage extends StatefulWidget {
//   @override
//   _DownloadsPageState createState() => _DownloadsPageState();
// }

// class _DownloadsPageState extends State<DownloadsPage> {
//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       backgroundColor: Colors.grey[900],
//       appBar: AppBar(
//         backgroundColor: Colors.grey[850],
//         title: Text("Downloads"),
//       ),
//       body: SeriesGrid(
//         future: DBManager.loadDownloaded(),
//         onNoResultsMessage:
//             "No Downloads\nTry downloading some series via Search",
//         canRemove: true,
//         refresher: () {
//           setState(() {});
//         },
//       ),
//     );
//   }
// }
