import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';

import 'package:scraper/logic/scraper_interfaces.dart';
import 'package:scraper/widgets/series_panel.dart';

class SeriesGrid extends StatelessWidget {
  final Future<List<Series>> future;
  final String onErrorMessage;
  final String onNoResultsMessage;
  final bool canRemove;
  final bool canSubscribe;
  final Function? refresher;

  SeriesGrid({
    required this.future,
    this.onErrorMessage = "Something went wrong X(",
    this.onNoResultsMessage = "No results :/",
    this.canRemove = false,
    this.canSubscribe = false,
    this.refresher,
  });

  @override
  Widget build(BuildContext context) {
    return FutureBuilder<List<Series>>(
      future: this.future,
      initialData: null,
      builder: (context, snapshot) {
        if (snapshot.hasError)
          return Center(
            child: Text(
              this.onErrorMessage,
              style: TextStyle(
                color: Colors.grey[400],
              ),
              textAlign: TextAlign.center,
            ),
          );
        if (snapshot.data == null) // show loading screen
          return Center(
            child: SpinKitCubeGrid(
              color: Colors.blue[400],
              size: 40.0,
            ),
          );
        if (!snapshot.hasData || snapshot.data!.length == 0)
          return Center(
            child: Text(
              this.onNoResultsMessage,
              style: TextStyle(
                color: Colors.grey[400],
              ),
              textAlign: TextAlign.center,
            ),
          );
        else
          return generateGrid(snapshot.data!, canRemove, canSubscribe);
      },
    );
  }

  static Widget generateGrid(
      List<Series> results, bool canRemove, bool canSubscribe) {
    return GridView.builder(
      itemCount: results.length,
      gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
        crossAxisCount: 3,
        childAspectRatio: 0.6,
        crossAxisSpacing: 5.0,
        mainAxisSpacing: 5.0,
      ),
      itemBuilder: (context, index) {
        return SeriesPanel(
            series: results[index],
            canRemove: canRemove,
            canSubscribe: canSubscribe,
            refresher: null,);
      },
    );
  }
}
