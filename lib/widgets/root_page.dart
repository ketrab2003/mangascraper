import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import 'package:scraper/widgets/search/search_page.dart';
import 'package:scraper/widgets/history_page.dart';
import 'package:scraper/widgets/favourites_page.dart';
import 'package:scraper/widgets/settings_page.dart';
import 'package:scraper/widgets/navigation_bar.dart';

import 'package:scraper/logic/downloader.dart';
import 'package:scraper/logic/db_manager.dart';

import 'package:scraper/logic/update_checker.dart';

class RootPage extends StatefulWidget {
  @override
  _RootPageState createState() => _RootPageState();
}

class _RootPageState extends State<RootPage> {
  int _selectedPageIndex = 1;

  PageController controller = PageController(initialPage: 1);

  @override
  void initState() {
    super.initState();
    SystemChrome.setEnabledSystemUIOverlays(
        SystemUiOverlay.values); // show status bar
    //tasks.init(); // start chapter checking service
    UpdateChecker.requestPermission();
    Downloader.tryToBackup();
    Downloader.checkAllForNewChapters();
    UpdateChecker.removeOldInstaller();
    DBManager.integrityCheck();
  }

  @override
  void dispose() {
    controller.dispose();

    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: PageView(
        children: <Widget>[
          SearchPage(),
          HistoryPage(),
          FavouritesPage(),
          ///DownloadsPage(), // TODO: add download option
          SettingsPage(),
        ],
        controller: controller,
        onPageChanged: (index) {
          setState(() {
            _selectedPageIndex = index;
          });
        },
      ),
      floatingActionButton: FutureBuilder<bool>(
        future: UpdateChecker.isUpdate,
        initialData: false,
        builder: (context, snapshot) => Visibility(
            visible: snapshot.data!,
            child: FloatingActionButton(
              child: Icon(Icons.update),
              onPressed: () => UpdateChecker.showUpdateDialog(context),
            )),
      ),
      bottomNavigationBar: CustomNavigationBar(
        index: _selectedPageIndex,
        controller: controller,
      ),
    );
  }
}
