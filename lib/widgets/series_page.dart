import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:flutter/services.dart';
import 'package:scraper/logic/db_manager.dart';

import 'package:scraper/logic/scraper_interfaces.dart';
import 'package:scraper/logic/scraper_manager.dart';
import 'package:scraper/widgets/chapter_viewer/chapter_viewer.dart';

class SeriesPage extends ConsumerWidget {
  final ValueNotifier<int> refresh = ValueNotifier<int>(0);

  final Series series;
  final bool canRemove;
  final bool canSubscribe;

  SeriesPage(
      {required this.series,
      this.canRemove = false,
      this.canSubscribe = false});

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final seriesState = ref.watch(seriesProvider(series));
    final seriesNotifier = ref.watch(seriesProvider(series).notifier);

    return Scaffold(
      backgroundColor: Colors.grey[900],
      appBar: AppBar(
        backgroundColor: Colors.grey[850],
        leading: BackButton(
          onPressed: () => Navigator.pop(context),
        ),
        title: Text(seriesState.title),
      ),
      endDrawer: Drawer(
        child: ListView(
          padding: EdgeInsets.zero,
          children: [
            DrawerHeader(
              child: Text(
                "Options",
                style: TextStyle(
                  color: Colors.grey[350],
                  fontSize: 20,
                ),
              ),
              decoration: BoxDecoration(
                color: Colors.grey[700],
              ),
            ),
            ListTile(
              title: Text(
                "Remove from device",
                style: TextStyle(
                  color: Colors.grey[400],
                ),
              ),
              onTap: () async {
                await DBManager.removeSeries(series);
                refresh.value++;
                Navigator.pop(context);
              },
            ),
            /*Theme(
                data: ThemeData(
                  textTheme: TextTheme(
                    bodyText1: TextStyle(color: Colors.grey[400]),
                    caption: TextStyle(color: Colors.grey[500]),
                  ),
                ),
                child: SwitchSettingsTile(
                  settingKey: "check-for-${Slugify(series.url)}",
                  defaultValue: true,
                  title: "Auto download new chapters",
                  subtitle: "Setting only for this series",
                )),*/
          ],
        ),
      ),
      body: FutureBuilder(future: () async {
        if (seriesState.chapters != null) return true;
        Series newSeriesState =
            await ScraperManager.updateSeriesWithChapters(series);
        if (await DBManager.isInDb(newSeriesState.url)) {
          newSeriesState = await DBManager.saveSeries(newSeriesState,
              metadataOnly: true, noProgressUpdate: true);
        }
        seriesNotifier.updateState(newSeriesState);
        return false;
      }(), builder: (context, snapshot) {
        if (!snapshot.hasData || snapshot.data == false)
          return ListView(
            padding: EdgeInsets.symmetric(vertical: 10.0),
            children: <Widget>[
              Hero(
                tag: series.url,
                child: Container(
                  height: 200.0,
                  decoration: BoxDecoration(
                    image: DecorationImage(
                      image: seriesState.cover,
                      fit: BoxFit.contain,
                    ),
                  ),
                ),
              ),
              Padding(
                padding: EdgeInsets.symmetric(vertical: 10.0),
                child: Center(
                  child: Text(
                    seriesState.title,
                    style: TextStyle(
                      fontSize: 15,
                      color: Colors.grey[300],
                    ),
                    textAlign: TextAlign.center,
                  ),
                ),
              ),
              Center(
                child: SpinKitCubeGrid(
                  color: Colors.blue[400],
                  size: 40.0,
                ),
              ),
            ],
          );

        return ListView(
            padding: EdgeInsets.symmetric(vertical: 10.0),
            children: <Widget>[
              // cover
              Hero(
                tag: series.url,
                child: Container(
                  height: 200.0,
                  decoration: BoxDecoration(
                    image: DecorationImage(
                      image: seriesState.cover,
                      fit: BoxFit.contain,
                    ),
                  ),
                ),
              ),
              // title
              Padding(
                padding: EdgeInsets.symmetric(vertical: 10.0),
                child: Center(
                  child: Text(
                    seriesState.title,
                    style: TextStyle(
                      fontSize: 15,
                      color: Colors.grey[300],
                    ),
                    textAlign: TextAlign.center,
                  ),
                ),
              ),
              // series info
              Center(
                child: Text(
                  seriesState.author +
                      " • " +
                      seriesState.status +
                      " • " +
                      seriesState.scraper,
                  style: TextStyle(
                    fontSize: 13,
                    color: Colors.grey[500],
                  ),
                  textAlign: TextAlign.center,
                ),
              ),
              // series bar (action buttons)
              SeriesBar(
                series: series,
                canRemove: canRemove,
                canSubscribe: canSubscribe,
                refresh: refresh,
              ),
              // chapters
              ChapterList(
                series: series,
                canSubscribe: canSubscribe,
                refresh: refresh,
              ),
            ]);
      }),
    );
  }
}

class ChapterList extends ConsumerWidget {
  final Series series;
  final bool canSubscribe;
  final ValueNotifier<int> refresh;

  ChapterList(
      {required this.series,
      required this.canSubscribe,
      required this.refresh});

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final seriesState = ref.watch(seriesProvider(series));

    return ValueListenableBuilder(
        valueListenable: refresh,
        builder: (context, int _, child) {
          return Column(
            children: List<int>.generate(seriesState.chapters!.length, (i) => i)
                .reversed
                .toList()
                .map(
                  (index) => ListTile(
                    title: Text(
                      seriesState.chapters![index].title,
                      style: TextStyle(
                        fontSize: 14,
                        color: (seriesState.chapters![index].read
                            ? Colors.green[200]
                            : Colors.grey[500]),
                      ),
                    ),
                    onTap: () async {
                      // open chapter to read
                      await Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => ChapterViewer(
                                    series: seriesState,
                                    chapterIndex: index,
                                    refresh: () => refresh.value++,
                                  )));
                      SystemChrome.setEnabledSystemUIOverlays(
                          SystemUiOverlay.values);
                    },
                    //trailing: ChapterButton(widget.series, index),  // TODO: add download option
                  ),
                )
                .toList(),
          );
        });
  }
}

// class ChapterButton extends StatefulWidget {
//   final Series series;
//   final int index;

//   ChapterButton(this.series, this.index);

//   @override
//   _ChapterButtonState createState() => _ChapterButtonState();
// }

// class _ChapterButtonState extends State<ChapterButton> {
//   @override
//   Widget build(BuildContext context) {
//     if (widget.series.chapters[widget.index].downloaded == true)
//       // remove button
//       return TextButton.icon(
//         onPressed: () async {
//           //await Downloader.removeChapter(widget.series, widget.index);  // TODO
//           setState(() {});
//         },
//         icon: Icon(Icons.delete, color: Colors.grey[400]),
//         label: Text(""),
//       );
//     if (widget.series.chapters[widget.index].downloading == true)
//       // downloading button
//       return TextButton.icon(
//         onPressed: () {},
//         icon: Icon(Icons.cached, color: Colors.grey[400]),
//         label: Text(""),
//       );
//     if (widget.series.chapters[widget.index].queued == true)
//       // queued button
//       return TextButton.icon(
//         onPressed: () async {
//           //await Downloader.removeChapter(widget.series, widget.index);  // TODO
//           setState(() {});
//         },
//         icon: Icon(Icons.query_builder, color: Colors.grey[400]),
//         label: Text(""),
//       );
//     // download button
//     return TextButton.icon(
//       onPressed: () async {
//         //await Downloader.queueChapter(widget.series, widget.index);  // TODO
//         setState(() {});
//       },
//       icon: Icon(Icons.arrow_downward, color: Colors.grey[400]),
//       label: Text(""),
//     );
//   }
// }

class LabelUnderIconButton extends StatelessWidget {
  final Widget label;
  final Widget icon;
  final void Function() onPressed;

  LabelUnderIconButton(
      {required this.label, required this.icon, required this.onPressed});

  @override
  Widget build(BuildContext context) {
    return FlatButton(
      disabledColor: Colors.grey[800],
      onPressed: this.onPressed,
      child: Column(
        children: <Widget>[
          this.icon,
          this.label,
        ],
      ),
    );
  }
}

class SeriesBar extends ConsumerWidget {
  final Series series;
  final bool canRemove;
  final bool canSubscribe;

  final ValueNotifier<int> refresh;

  SeriesBar(
      {required this.series,
      required this.canRemove,
      required this.canSubscribe,
      required this.refresh});

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final seriesState = ref.watch(seriesProvider(series));
    final seriesNotifier = ref.watch(seriesProvider(series).notifier);

    return Center(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: <Widget>[
              // start reading
              LabelUnderIconButton(
                onPressed: () async {
                  Series loadedSeries =
                      await DBManager.updateSeriesWithUserData(seriesState);
                  int last = loadedSeries.lastChapter;

                  // move to first unread chapter
                  if (last == -1) {
                    last++;
                    while (loadedSeries.chapters![last].read) last++;
                  }

                  int lastPage = loadedSeries.lastPage;

                  await Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => ChapterViewer(
                                series: loadedSeries,
                                chapterIndex: last,
                                pageIndex: lastPage,
                                refresh: () => refresh.value++,
                              )));
                  SystemChrome.setEnabledSystemUIOverlays(
                      SystemUiOverlay.values);
                  refresh.value++;
                },
                icon: Icon(Icons.play_arrow, color: Colors.grey[400]),
                label: Text(
                  "Read",
                  style: TextStyle(color: Colors.grey[400]),
                ),
              ),
              // webtoon
              LabelUnderIconButton(
                onPressed: () async {
                  var newSeriesState =
                      seriesState.copyWith(isWebtoon: !seriesState.isWebtoon);
                  newSeriesState = await DBManager.saveSeries(newSeriesState,
                      metadataOnly: true, noProgressUpdate: true);
                  seriesNotifier.updateState(newSeriesState);
                },
                icon: Icon(
                  (seriesState.isWebtoon
                      ? Icons.text_snippet
                      : Icons.text_snippet_outlined),
                  color: Colors.grey[400],
                ),
                label: Text(
                  "Webtoon mode",
                  style: TextStyle(color: Colors.grey[400]),
                ),
              ),
              // favourite
              IconButton(
                onPressed: () async {
                  var newSeriesState =
                      seriesState.copyWith(favourite: !seriesState.favourite);
                  newSeriesState = await DBManager.saveSeries(newSeriesState,
                      metadataOnly: true, noProgressUpdate: true);
                  seriesNotifier.updateState(newSeriesState);
                },
                splashRadius: 0.0001,
                icon: Icon(
                  (seriesState.favourite
                      ? Icons.favorite
                      : Icons.favorite_border),
                  color: Colors.grey[400],
                  size: 40,
                ),
              ),
              // download
              /*CustomDownloadButton(
                series: widget.series,
              ),*/ // TODO: add download option
            ] +
            (canRemove
                ? [
                    LabelUnderIconButton(
                      onPressed: () {
                        //Downloader.removeSeries(series: widget.series);  // TODO
                        Navigator.of(context).pop();
                      },
                      icon: Icon(Icons.delete, color: Colors.grey[400]),
                      label: Text(
                        "Remove",
                        style: TextStyle(color: Colors.grey[400]),
                      ),
                    ),
                  ]
                : []),
      ),
    );
  }
}

/*class CustomDownloadButton extends StatelessWidget {
  final toon.Series series;

  CustomDownloadButton({this.series});

  @override
  Widget build(BuildContext context) {
    return StreamBuilder<double>(
      stream: series.downloadProgress,
      builder: (_, snapshot) {
        if (!snapshot.hasData)
          // no data
          return CircularProgressIndicator();
        if (snapshot.data == -100)
          // series isn't downloaded
          return LabelUnderIconButton(
            onPressed: () => null, //Downloader.queueSeries(series),  // TODO
            icon: Icon(Icons.cloud_download, color: Colors.grey[400]),
            label: Text(
              "Download",
              style: TextStyle(color: Colors.grey[400]),
            ),
          );
        if (snapshot.data == -0.1)
          // series is queued but not downloaded
          return LabelUnderIconButton(
            onPressed: () {},
            icon: Icon(Icons.timer, color: Colors.grey[400]),
            label: Text(
              "Queued",
              style: TextStyle(color: Colors.grey[400]),
            ),
          );
        if (snapshot.data == 1.0)
          // series is completely downloaded
          return LabelUnderIconButton(
            onPressed: () {},
            icon: Icon(Icons.cloud_done, color: Colors.green[400]),
            label: Text(
              "Downloaded",
              style: TextStyle(color: Colors.grey[400]),
            ),
          );
        // series is downloading
        return LabelUnderIconButton(
          onPressed: () {},
          icon: CircularProgressIndicator(
            value: snapshot.data,
          ),
          label: Text(
            "Downloading... ${(snapshot.data * 100).round()}%",
            style: TextStyle(color: Colors.grey[400]),
          ),
        );
      },
    );
  }
}
*/
