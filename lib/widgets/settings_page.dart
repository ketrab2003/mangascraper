import 'package:flutter/material.dart';
import 'package:package_info/package_info.dart';

import 'package:scraper/logic/db_manager.dart';
import 'package:scraper/logic/gdrive_api.dart';
import 'package:scraper/logic/settings.dart';

import 'package:flutter_riverpod/flutter_riverpod.dart';

final appVersionProvider = FutureProvider<String>((ref) async {
  return (await PackageInfo.fromPlatform()).version;
});

class SettingsPage extends ConsumerWidget {
  const SettingsPage({Key? key}) : super(key: key);

  static const Map<String, String> frequencyOptions = {
    '15 minutes': 'm',
    'hour': 'h',
    'day': 'd',
    'week': 'w',
  };

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final appVersion = ref.watch(appVersionProvider);
    final settings = ref.watch(settingsProvider);

    return Scaffold(
      backgroundColor: Colors.grey[900],
      appBar: AppBar(
        backgroundColor: Colors.grey[850],
        title: Text("Settings"),
      ),
      body: ListView(
        children: [
          // --- app version ---
          ListTile(
            title: Text("App version"),
            subtitle: Text(
              appVersion.when(
                  data: (data) => data,
                  error: (e, s) => "Something went wrong",
                  loading: () => "Loading..."),
            ),
          ),
          // --- mobile data usage ---
          SwitchListTile(
            title: Text("Allow usage of mobile data"),
            subtitle: Text(
                "When enabled app will be downloading large files via mobile data when wi-fi is disabled"),
            value: settings.when(
                data: (d) => d.getBool('allow-mobile', false),
                error: (e, s) => false,
                loading: () => false),
            onChanged: (val) {
              settings.whenData((d) => d.save('allow-mobile', val));
              ref.refresh(settingsProvider);
            },
          ),
          // --- checking interval ---
          Container(height: 10),
          ListTile(
            title: Text("Check for new chapters every"),
            subtitle: DropdownButtonHideUnderline(
              child: DropdownButton(
                style: TextStyle(
                    color: Theme.of(context).textTheme.headline2!.color),
                items: frequencyOptions
                    .map((description, value) {
                      return MapEntry(
                          description,
                          DropdownMenuItem<String>(
                            value: value,
                            child: Text(description),
                          ));
                    })
                    .values
                    .toList(),
                value: settings.when(
                  data: (d) => d.getString('downloaded-interval', 'm'),
                  error: (e, s) => 'm',
                  loading: () => 'm',
                ),
                onChanged: (val) {
                  settings.whenData((d) => d.save('downloaded-interval', val));
                  ref.refresh(settingsProvider);
                },
              ),
            ),
          ),
          // --- google drive ---
          settings.when(
            data: (d) {
              if (d.getBool('drive-enabled', false))
                return ListTile(
                  title: Text("Log out of Google Drive"),
                  subtitle: Text("It will not remove backed up progress"),
                  onTap: () async {
                    await GoogleAuthClient.logOut();
                    final snackBar = SnackBar(content: Text('Logged out!'));
                    ScaffoldMessenger.of(context).showSnackBar(snackBar);
                    settings.whenData((d) => d.save('drive-enabled', false));
                    ref.refresh(settingsProvider);
                  },
                );
              else
                return ListTile(
                  title: Text("Use Google Drive to store data"),
                  onTap: () async {
                    await DBManager.loadBackupData();
                    final snackBar = SnackBar(content: Text('Logged in!'));
                    ScaffoldMessenger.of(context).showSnackBar(snackBar);
                    settings.whenData((d) => d.save('drive-enabled', true));
                    ref.refresh(settingsProvider);
                  },
                );
            },
            error: (e, s) => ListTile(),
            loading: () => ListTile(),
          ),
        ],
      ),
    );
  }
}