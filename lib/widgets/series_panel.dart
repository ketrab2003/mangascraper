import 'package:flutter/material.dart';

import 'package:scraper/logic/scraper_interfaces.dart';
import 'package:scraper/widgets/series_page.dart';

class SeriesPanel extends StatelessWidget {
  final Series series;
  final bool canRemove;
  final bool canSubscribe;
  final Function? refresher;

  SeriesPanel(
      {required this.series,
      this.canRemove = false,
      this.canSubscribe = false,
      this.refresher});

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () async {
        await Navigator.push(
            context,
            MaterialPageRoute(
              builder: (context) => SeriesPage(
                series: series,
                canRemove: canRemove,
                canSubscribe: canSubscribe,
              ),
            ));
        if (refresher != null) refresher!();
      },
      child: FutureBuilder<bool>(
          future: () async {
            return (series.lastReadData != -1);
          }(),
          initialData: false,
          builder: (context, snapshot) => Container(
                height: 190.0,
                width: 105.0,
                color: (snapshot.data! ? Colors.grey[700] : Colors.grey[800]),
                child: Column(
                  children: <Widget>[
                    Expanded(
                      child: Padding(
                        padding: EdgeInsets.only(bottom: 5.0),
                        child: Hero(
                          tag: series.url,
                          child: Image(
                            image: series.cover,
                          ),
                        ),
                      ),
                    ),
                    Container(
                      height: 24.0,
                      child: Padding(
                        padding:
                            EdgeInsets.symmetric(vertical: 0, horizontal: 5.0),
                        child: Text(
                          series.title,
                          overflow: TextOverflow.ellipsis,
                          maxLines: 2,
                          style: TextStyle(
                            color: Colors.grey[300],
                            fontSize: 11.0,
                          ),
                        ),
                      ),
                    ),
                    Container(
                      color: Colors.transparent,
                      height: 10.0,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: [
                          Padding(
                            padding: EdgeInsets.symmetric(
                                vertical: 0, horizontal: 5.0),
                            child: Text(
                              series.scraper,
                              overflow: TextOverflow.ellipsis,
                              maxLines: 1,
                              textAlign: TextAlign.end,
                              style: TextStyle(
                                color: Colors.grey[300],
                                fontSize: 8.0,
                                backgroundColor: Colors.transparent,
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              )),
    );
  }
}
