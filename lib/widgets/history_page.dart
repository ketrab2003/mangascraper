import 'package:flutter/material.dart';

import 'package:scraper/logic/db_manager.dart';
import 'package:scraper/widgets/series_list.dart';

class HistoryPage extends StatefulWidget {
  @override
  _HistoryPageState createState() => _HistoryPageState();
}

class _HistoryPageState extends State<HistoryPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.grey[900],
      appBar: AppBar(
        backgroundColor: Colors.grey[850],
        title: Text("Last read"),
      ),
      body: SeriesList(
        future: DBManager.loadHistory(),
        onNoResultsMessage: "No history\nRead some series ;)",
        refresher: refresher,
        cacheKey: "history",
      ),
    );
  }

  void refresher() {
    setState(() {});
  }
}
