import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:scraper/logic/db_manager.dart';

import 'package:scraper/logic/scraper_interfaces.dart';
import 'package:scraper/widgets/chapter_viewer/chapter_viewer.dart';
import 'package:scraper/widgets/series_page.dart';
import 'package:flutter/services.dart';

class CachedSeriesData {
  static Map<String, List<Series>> _caches = <String, List<Series>>{};

  static List<Series>? getCache(String key) {
    if (_caches.containsKey(key)) return _caches[key]!;
    return null;
  }

  static void setCache(String key, List<Series> data) {
    _caches[key] = data;
  }
}

class SeriesList extends StatefulWidget {
  final Future<List<Series>> future;
  final String onErrorMessage;
  final String onNoResultsMessage;
  final Function? refresher;
  final String? cacheKey;

  SeriesList(
      {required this.future,
      this.onErrorMessage = "Something went wrong X(",
      this.onNoResultsMessage = "No results :/",
      this.refresher,
      this.cacheKey});

  @override
  _SeriesListState createState() => _SeriesListState();
}

class _SeriesListState extends State<SeriesList> {
  @override
  Widget build(BuildContext context) {
    return FutureBuilder<List<Series>>(
      future: widget.future,
      initialData: widget.cacheKey != null ? CachedSeriesData.getCache(widget.cacheKey!) : null,

      builder: (context, snapshot) {
        
        if (snapshot.hasError) {  // some error occured
          print(snapshot.error);
          return Center(
            child: Text(
              widget.onErrorMessage,
              style: TextStyle(
                color: Colors.grey[400],
              ),
              textAlign: TextAlign.center,
            ),
          );
        }

        if (snapshot.data == null) // show loading screen
          return Center(
            child: SpinKitCubeGrid(
              color: Colors.blue[400],
              size: 40.0,
            ),
          );

        if (!snapshot.hasData || snapshot.data!.length == 0) // no resulst
          return Center(
            child: Text(
              widget.onNoResultsMessage,
              style: TextStyle(
                color: Colors.grey[400],
              ),
              textAlign: TextAlign.center,
            ),
          );
        
        // got data
        if (widget.cacheKey != null)
          CachedSeriesData.setCache(widget.cacheKey!, snapshot.data!);
        return generateList(snapshot.data!);
      },
    );
  }

  Widget generateList(List<Series> results) {
    return ListView.builder(
      itemCount: results.length,
      itemBuilder: (context, index) => ListTile(
        leading: Hero(
            tag: results[index].url,
            child: Image(
              image: results[index].cover,
              height: 50,
            )),
        title: Text(
          results[index].title,
          style: TextStyle(color: Colors.grey[400]),
        ),
        subtitle: Text(
          results[index].author + "\n" + results[index].scraper,
          style: TextStyle(color: Colors.grey[500]),
        ),
        trailing: Row(
          mainAxisSize: MainAxisSize.min,
          children: [
            // new chapter marker
            if (results[index].newChapters != -1 &&
                results[index].newChapters < results[index].nofChapters)
              Container(
                alignment: Alignment.center,
                decoration: BoxDecoration(
                  color: Colors.orange,
                  shape: BoxShape.circle,
                ),
                child: Text(
                    "${results[index].nofChapters - results[index].newChapters}"),
                height: 30,
                width: 30,
              ),
            SizedBox(
              width: 10,
            ),
            GestureDetector(
              onTap: () async {       // open series' page
                await Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => SeriesPage(
                        series: results[index],
                        canRemove: false,
                        canSubscribe: true,
                      ),
                    ));
                if (widget.refresher != null) widget.refresher!();
              },
              child: Icon(
                Icons.info,
                color: Colors.grey[400],
              ),
            ),
          ],
        ),
        onTap: () async {     // open chapter viewer
          if(results[index].chapters == null)
            results[index] = await DBManager.loadSeries(results[index].url);

          int last = results[index].lastChapter;

          // move to first unread chapter
          if (last == -1) {
            last = results[index].chapters!.indexWhere((e) => !e.read);
          }

          int lastPage = results[index].lastPage;

          await Navigator.push(
              context,
              MaterialPageRoute(
                  builder: (context) => ChapterViewer(
                        series: results[index],
                        chapterIndex: last,
                        pageIndex: lastPage,
                      )));
          await SystemChrome.setEnabledSystemUIOverlays(SystemUiOverlay.values);
          if (widget.refresher != null) widget.refresher!();
        },
      ),
    );
  }
}
