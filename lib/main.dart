import 'package:flutter/material.dart';
import 'package:scraper/widgets/root_page.dart';

import 'package:flutter_riverpod/flutter_riverpod.dart';

//import 'package:workmanager/workmanager.dart';

void main() async {
  //Workmanager.cancelAll();

  runApp(ProviderScope(
      child: MaterialApp(
    home: RootPage(),
    theme: ThemeData.dark(),
  )));
}