import 'dart:convert';

import 'package:scraper/logic/cache_classes.dart';
import 'package:dio/dio.dart';
import 'package:dio_http_cache/dio_http_cache.dart';

import 'package:scraper/logic/scraper_interfaces.dart';

class Hub extends IScraper {
  bool recognize(String url) {
    return RegExp(r'^http(s|)://mangahub.io/manga/.+$').hasMatch(url);
  }

  String name() => 'MangaHub.io';

  Future<List<Series>> search(String term) async {
    var r = await DioWithCache.instance.post(
      "https://api.mghubcdn.com/graphql",
      options: Options(headers: {"Content-Type": "application/json"}),
      data: jsonEncode({
        'query': '{search(x:m01,q:"$term",limit:30){rows{title,slug,image}}}'
      }),
    );

    if (r.statusCode != 200) return [];

    var data = r.data;

    return data['data']['search']['rows']
        .map<Series>((series) => Series(
              url: "https://mangahub.io/manga/${series['slug']}",
              title: series['title'],
              coverUrl: "https://thumb.mghubcdn.com/${series['image']}",
              author: '',
              status: '',
              scraper: this.name(),
            ))
        .toList();
  }

  Future<Series> getSeries(String url) async {
    String slug = url.split('/').last;
    var page = await DioWithCache.instance.post(
      "https://api.mghubcdn.com/graphql",
      options: buildCacheOptions(Duration(days: 30),
          forceRefresh: true,
          options: Options(headers: {"Content-Type": "application/json"})),
      data: jsonEncode(
          {'query': '{manga(x:m01,slug:"$slug"){title,image,status,author}}'}),
    );
    var data = page.data['data']['manga'];

    return Series(
      url: url,
      title: data['title'],
      coverUrl: 'https://thumb.mghubcdn.com/${data["image"]}',
      author: data['author'],
      status: data['status'],
      scraper: this.name(),
    );
  }

  Future<Series> addChapters(Series series) async {
    String slug = series.url.split('/').last;
    var page = await DioWithCache.instance.post(
      "https://api.mghubcdn.com/graphql",
      options: buildCacheOptions(Duration(days: 30),
          forceRefresh: true,
          options: Options(headers: {"Content-Type": "application/json"})),
      data: jsonEncode(
          {'query': '{manga(x:m01,slug:"$slug"){chapters{number,title}}}'}),
    );
    var data = page.data['data']['manga'];

    List<Chapter> chapters = [];
    for (var chapter in data["chapters"]) {
      chapters.add(Chapter(
        seriesUrl: series.url,
        url: "https://mangahub.io/chapter/$slug/chapter-${chapter['number']}",
        title: "Chapter ${chapter['number']} : ${chapter['title']}",
        ord: chapters.length,
      ));
    }

    return series.copyWith(chapters: chapters);
  }

  Future<Chapter> addPages(Chapter chapter) async {
    String slug = chapter.seriesUrl.split('/').last;
    if(chapter.url.isEmpty) {
      String chapterNum = RegExp(r'Chapter ([\d.]+) : ').allMatches(chapter.title).first.group(1)!;
      chapter = chapter.copyWith(url: 'https://mangahub.io/chapter/$slug/chapter-$chapterNum');
    }
    String chapternum = chapter.url.split('-').last;

    var r = await DioWithCache.instance.post(
      "https://api.mghubcdn.com/graphql",
      options: Options(headers: {"Content-Type": "application/json"}),
      data: jsonEncode(
          {'query': '{chapter(x:m01,slug:"$slug",number:$chapternum){pages}}'}),
    );

    var data = jsonDecode(r.data['data']['chapter']['pages']);

    List<String> pages = await data.values
        .map<String>((link) => "https://img.mghubcdn.com/file/imghub/$link")
        .toList();
    
    return chapter.copyWith(pages: pages);
  }
}