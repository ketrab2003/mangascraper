import 'package:scraper/logic/cache_classes.dart';
import 'package:dio_http_cache/dio_http_cache.dart';

import 'package:scraper/logic/scraper_interfaces.dart';

T? cast<T>(x) => x is T ? x : null;

double parseNumber(String v) {
  var a = double.tryParse(v);
  if (a == null) {
    var found = RegExp(r'^\d+(\.\d+|)').allMatches(v);
    if (found.length > 0) {
      a = double.tryParse(found.first.group(0)!);
    }
  }
  return (a == null ? 0 : a);
}

class Dex extends IScraper {
  bool recognize(String url) {
    return RegExp(r'^http(s|)://mangadex.org/title/.+$').hasMatch(url);
  }

  String name() => 'MangaDex.org';

  Future<List<Series>> search(String term) async {
    term = Uri.encodeComponent(term);
    var r = await DioWithCache.instance.get(
        'https://api.mangadex.org/manga?title=$term&limit=30&contentRating[]=safe&contentRating[]=suggestive&contentRating[]=erotica&includes[]=author&includes[]=cover_art');

    if (r.statusCode != 200) return [];

    var data = r.data;

    return data['data'].map<Series>((series) {
      // search for cover
      String coverSlug = '';
      for (var relationship in series['relationships']) {
        if (relationship['type'] == 'cover_art') {
          coverSlug = relationship['attributes']['fileName'];
          break;
        }
      }

      return Series(
        scraper: this.name(),
        url: "https://mangadex.org/title/${series['id']}",
        title: series['attributes']['title']['en'],
        coverUrl:
            'https://uploads.mangadex.org/covers/${series["id"]}/$coverSlug',
        author: '',
        status: '',
      );
    }).toList();
  }

  Future<Series> getSeries(String url) async {
    String id = url.split('/').last;
    var page = await DioWithCache.instance.get(
      'https://api.mangadex.org/manga/$id?includes[]=artist&includes[]=author&includes[]=cover_art',
      options: buildCacheOptions(Duration(days: 30), forceRefresh: true),
    );

    var data = page.data['data'];

    // find author data
    String author = '';
    String coverSlug = '';
    for (var relationship in data['relationships']) {
      if (relationship['type'] == 'author' && author.isEmpty) {
        author = relationship['attributes']['name'];
      }
      if (relationship['type'] == 'cover_art' && coverSlug.isEmpty) {
        coverSlug = relationship['attributes']['fileName'];
      }
      if(author.isNotEmpty && coverSlug.isNotEmpty) {
        break;
      }
    }

    return Series(
      url: 'https://mangadex.org/title/${data["id"]}',
      title: data['attributes']['title']['en'],
      author: author,
      status: data['attributes']['status'],
      coverUrl: 'https://uploads.mangadex.org/covers/${data["id"]}/$coverSlug',
      scraper: this.name(),
    );
  }

  Future<Series> addChapters(Series series) async {
    String id = series.url.split('/').last;

    List<dynamic> rawChapters = [];

    int totalChapters = 101, offset = 0;

    while (offset < totalChapters) {
      var page = await DioWithCache.instance.get(
        "https://api.mangadex.org/chapter?manga=$id&limit=100&offset=$offset",
        options: buildCacheOptions(Duration(days: 30), forceRefresh: true),
      );
      var data = page.data;
      totalChapters = data['total'];

      rawChapters.addAll(data['data']
          .where((f) => (f['attributes']['translatedLanguage'] == 'en')));

      offset += 100;
    }

    rawChapters.sort((a, b) {
      String an = a['attributes']['chapter'].toString();
      String bn = b['attributes']['chapter'].toString();
      var av = parseNumber(an);
      var bv = parseNumber(bn);

      if (av == bv) {
        return an.compareTo(bn);
      }
      return av.compareTo(bv);
    });

    // old url scheme: "https://api.mangadex.org/chapter?chapter=${chapter['attributes']['chapter']}&manga=$id&translatedLanguage[]=en"
    int i = 0;
    List<Chapter> chapters = rawChapters
        .map((chapter) => Chapter(
              seriesUrl: series.url,
              url: 'https://api.mangadex.org/at-home/server/${chapter["id"]}?forcePort443=false',
              title: (chapter['attributes']['chapter'] == null
                  ? "${chapter['attributes']['title']}"
                  : "Chapter ${chapter['attributes']['chapter']} : ${chapter['attributes']['title']}"),
              ord: i++,
            ))
        .toList();

    return series.copyWith(chapters: chapters);
  }

  Future<Chapter> addPages(Chapter chapter, {bool noCache = false}) async {

    // handle old urls
    if(chapter.url.startsWith('https://api.mangadex.org/chapter')) {
      var r = await DioWithCache.instance.get(chapter.url);
      var data = r.data['data'][0];
      String newUrl = 'https://api.mangadex.org/at-home/server/${data["id"]}?forcePort443=false';
      chapter = chapter.copyWith(url: newUrl);
    }

    var r = await DioWithCache.instance.get(chapter.url);
    var data = r.data;
    List<String> pages = data['chapter']['data']
        .map<String>(
            (v) => '${data["baseUrl"]}/data/${data["chapter"]["hash"]}/$v')
        .toList();
    return chapter.copyWith(pages: pages);
  }
}