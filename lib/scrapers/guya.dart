import 'package:dio_http_cache/dio_http_cache.dart';
import 'package:scraper/logic/cache_classes.dart';

import 'package:scraper/logic/scraper_interfaces.dart';

class Guya extends IScraper {
  bool recognize(String url) {
    return RegExp(r'^http(s|)://guya.moe/api/series/.+$').hasMatch(url);
  }

  String name() => 'Guya.moe';

  Future<List<Series>> search(String term) async {
    term = term.toLowerCase();

    if (term.contains("kaguya") ||
        (term.contains("love") && term.contains("war"))) {
      return [
        await this.getSeries(
            'https://guya.moe/api/series/Kaguya-Wants-To-Be-Confessed-To/?nocache'),
        await this.getSeries(
            'https://guya.moe/api/series/We-Want-To-Talk-About-Kaguya/?nocache'),
        await this.getSeries(
            'https://guya.moe/api/series/Kaguya-Wants-To-Be-Confessed-To-Official-Doujin/?nocache'),
      ];
    }

    if (term.contains('oshi') || (term.contains('no') && term.contains('ko'))) {
      return [
        await this.getSeries('https://guya.moe/api/series/Oshi-no-Ko/?nocache'),
      ];
    }

    return [];
  }

  Future<Series> getSeries(String url) async {
    var page = await DioWithCache.instance.get(url,
        options: buildCacheOptions(Duration(days: 30), forceRefresh: true));
    dynamic data = page.data;
    return Series(
        url: url,
        title: data['title'],
        author: data['author'],
        status: 'ongoing',
        coverUrl: 'https://guya.moe${data["cover"]}',
        scraper: this.name());
  }

  Future<Series> addChapters(Series series) async {
    var page = await DioWithCache.instance.get(series.url,
        options: buildCacheOptions(Duration(days: 30), forceRefresh: true));
    dynamic data = page.data;

    List<Chapter> chapters = [];

    int i = 0;
    await data['chapters'].forEach((key, value) => chapters.add(Chapter(
          seriesUrl: series.url,
          url: '${series.url}?chapter=$key',
          title: "Chapter $key: ${value['title']}",
          ord: i++,
        )));

    return series.copyWith(chapters: chapters);
  }

  Future<Chapter> addPages(Chapter chapter, {bool noCache = false}) async {
    if (chapter.url.isEmpty) {
      String key =
          RegExp(r'Chapter (\w+): ').allMatches(chapter.title).first.group(1)!;
      chapter = chapter.copyWith(url: '${chapter.seriesUrl}?chapter=$key');
    }
    
    String chapterId =
        RegExp(r'\?chapter=(\w+)$').firstMatch(chapter.url)?.group(1) as String;
    var page = await DioWithCache.instance.get(chapter.seriesUrl,
        options: buildCacheOptions(Duration(days: 30), forceRefresh: noCache));
    dynamic dataFull = page.data;
    dynamic data = dataFull['chapters'][chapterId];

    String groupName = data["groups"].keys.toList()[0];
    List<String> pages = data["groups"][groupName]
        .map<String>((img) =>
            'https://guya.moe/media/manga/${dataFull["slug"]}/chapters/${data["folder"]}/$groupName/$img')
        .toList();
    return chapter.copyWith(pages: pages);
  }
}
